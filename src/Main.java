import core.Path;
import core.WarehouseMap;
import core.impl.DefaultMapInitializer;
import tools.Exporter;
import tools.Test;


public class Main {

    public static void main(String[] args) {
        Test test = new Test();
        WarehouseMap warehouseMap = new WarehouseMap(new DefaultMapInitializer(3, 18));
        Exporter.export(warehouseMap.getMapObjectGraph(), "map.dot");
        Path path = warehouseMap.buildPath("a0c4", "a2c4");
    }
}
