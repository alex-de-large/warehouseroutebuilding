package tools;

import core.MapObject;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.nio.dot.DOTExporter;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Exporter {

    private Exporter() {

    }

    public static void export(Graph<MapObject, DefaultWeightedEdge> graph, String fname) {
        DOTExporter<MapObject, DefaultWeightedEdge> exporter = new DOTExporter<>(
                mapObject -> String.format("\"%s\"", mapObject.getId())
        );
        StringWriter writer = new StringWriter();
        exporter.exportGraph(graph, writer);

        try (PrintWriter out = new PrintWriter(fname)) {
            out.println(writer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
