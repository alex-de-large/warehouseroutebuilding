package core;

public class MapObject {

    public int n;
    public int alley;
    public float x;
    public float y;

    public MapObject(int n, int alley, float x, float y) {
        this.n = n;
        this.alley = alley;
        this.x = x;
        this.y = y;
    }

    public String getId() {
        return String.format("a%dc%d", alley, n);
    }

    public float distance(MapObject o) {
        float dx = o.x - this.x;
        float dy = o.y - this.y;
        return (float) Math.sqrt(dx * dx + dy * dy);
    }

    @Override
    public String toString() {
        return "MapObject{" +
                "n=" + n +
                ", alley=" + alley +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
