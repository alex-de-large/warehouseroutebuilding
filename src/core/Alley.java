package core;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.Arrays;

public class Alley {

    private int alleyIndex;
    private int n;
    private MapObject[][] mapObjects;
    private AlleyMovementDirection alleyMovementDirection;

    public static final float CELL_SIZE_X = 120f;
    public static final float CELL_SIZE_Y = 120f;
    public static final float ALLEY_WIDTH = 400f;

    public Alley(int alleyIndex, int nrow, AlleyMovementDirection alleyMovementDirection) {
        assert nrow > 0;
        this.alleyIndex = alleyIndex;
        this.n = nrow;
        this.mapObjects = new MapObject[nrow][2];
        this.alleyMovementDirection = alleyMovementDirection;
        init();
    }

    private void init() {
        int j = 1;
        float x = (ALLEY_WIDTH + CELL_SIZE_X * 2) * alleyIndex;
        for (int i = 0; i < n; i++, j += 2) {
            float mx = x + CELL_SIZE_X;
            float my = (i + 1) * CELL_SIZE_Y;
            mapObjects[i][0] = new MapObject(j, alleyIndex, mx, my);
            mapObjects[i][1] = new MapObject(j + 1, alleyIndex, mx + ALLEY_WIDTH, my);
//            System.out.println(Arrays.toString(mapObjects[i]));
        }
    }

    public MapObject topLeft() {
        return mapObjects[n - 1][0];
    }

    public MapObject topRight() {
        return mapObjects[n - 1][1];
    }

    public MapObject bottomLeft() {
        return mapObjects[0][0];
    }

    public MapObject bottomRight() {
        return mapObjects[0][1];
    }

    public Graph<MapObject, DefaultWeightedEdge> asGraph() {
        Graph<MapObject, DefaultWeightedEdge> graph = new SimpleDirectedWeightedGraph<>(DefaultWeightedEdge.class);

        MapObject first = null;
        MapObject second = null;
        for (int i = 0; i < n; i++) {

            graph.addVertex(mapObjects[i][0]);
            graph.addVertex(mapObjects[i][1]);

            if (i != 0) {
                if (alleyMovementDirection == AlleyMovementDirection.DOWNWARD) {
                    graph.addEdge(mapObjects[i][0], first);
                    graph.addEdge(mapObjects[i][1], second);
                } else {
                    graph.addEdge(first, mapObjects[i][0]);
                    graph.addEdge(second, mapObjects[i][1]);
                }
            }

            first = mapObjects[i][0];
            second = mapObjects[i][1];

            graph.addEdge(first, second);
            graph.addEdge(second, first);
        }

        return graph;
    }

    public MapObject[][] getMapObjects() {
        return mapObjects;
    }

    public AlleyMovementDirection getAlleyMovementDirection() {
        return alleyMovementDirection;
    }
}
