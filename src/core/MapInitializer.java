package core;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.Hashtable;

public interface MapInitializer {

    void initialize(MapObjectGraph mapObjectGraph, Hashtable<String, MapObject> mapObjectHashtable);
}
