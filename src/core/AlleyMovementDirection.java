package core;

public enum AlleyMovementDirection {
    DOWNWARD,
    UPWARD
}
