package core;

import org.jgrapht.Graph;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.Hashtable;
import java.util.List;

public class WarehouseMap {

    private Hashtable<String, MapObject> mapObjectHashtable;
    private MapObjectGraph mapObjectGraph;

    public WarehouseMap(MapInitializer initializer) {
        mapObjectGraph = new MapObjectGraph();
        mapObjectHashtable = new Hashtable<>();
        initializer.initialize(mapObjectGraph, mapObjectHashtable);
    }

    public Path buildPath(String id1, String id2) {
        DijkstraShortestPath<MapObject, DefaultWeightedEdge> dijkstraShortestPath
                = new DijkstraShortestPath<>(mapObjectGraph);
        List<MapObject> shortestPath = dijkstraShortestPath
                .getPath(
                        mapObjectHashtable.get(id1),
                        mapObjectHashtable.get(id2)
                )
                .getVertexList();
        return new Path(shortestPath);
    }


    public MapObjectGraph getMapObjectGraph() {
        return mapObjectGraph;
    }
}
