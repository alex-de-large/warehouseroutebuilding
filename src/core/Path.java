package core;

import java.util.ArrayList;
import java.util.List;

public class Path {

    private MapObject[] mapObjects;
    private int nextIndex;

    public Path(MapObject[] mapObjects) {
        this.mapObjects = mapObjects;
        optimize();
    }

    public Path(List<MapObject> mapObjects) {
        this(mapObjects.toArray(new MapObject[0]));
    }

    public MapObject getNext() {
        return mapObjects[nextIndex];
    }

    public void update(MapObject current) {
        if (current.equals(getNext()) && !isCompleted()) {
            nextIndex++;
        }
    }

    public boolean isCompleted() {
        return nextIndex + 1 != mapObjects.length;
    }

    private void optimize() {
        List<MapObject> optimized = new ArrayList<>();
        for (int i = 1; i < mapObjects.length - 1; i++) {
            MapObject prev = mapObjects[i - 1];
            MapObject current = mapObjects[i];
            MapObject next = mapObjects[i + 1];
//            if (prev.x != next.x && prev.y != next.y) {
            if (prev.alley != next.alley)
                optimized.add(current);
//            }
        }
        optimized.add(mapObjects[mapObjects.length - 1]);
        this.mapObjects = optimized.toArray(new MapObject[0]);
    }
}
