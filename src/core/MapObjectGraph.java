package core;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

public class MapObjectGraph extends SimpleDirectedWeightedGraph<MapObject, DefaultWeightedEdge> {

    public MapObjectGraph() {
        super(DefaultWeightedEdge.class);
    }

    @Override
    public DefaultWeightedEdge addEdge(MapObject sourceVertex, MapObject targetVertex) {
        DefaultWeightedEdge edge =  super.addEdge(sourceVertex, targetVertex);
        this.setEdgeWeight(edge, sourceVertex.distance(targetVertex));
        return edge;
    }
}
