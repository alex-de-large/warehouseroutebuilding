package core.impl;

import core.*;
import org.jgrapht.Graphs;

import java.util.Hashtable;

public class DefaultMapInitializer implements MapInitializer {

    private int nalley;
    private int nrow;

    public DefaultMapInitializer(int nalley, int nrow) {
        assert nrow % 2 == 0;
        this.nalley = nalley;
        this.nrow = nrow;
    }

    @Override
    public void initialize(MapObjectGraph mapObjectGraph, Hashtable<String, MapObject> mapObjectHashtable) {
        Alley[] alleys = createAlleys();

        for (Alley alley: alleys) {
            Graphs.addGraph(mapObjectGraph, alley.asGraph());
        }

//        for (int i = 0; i < alleyNum - 1; i++) {
//            if (alleys[i].getAlleyMovementDirection() == AlleyMovementDirection.DOWNWARD) {
//                mapObjectGraph.addEdge(
//                        alleys[i].bottomRight(),
//                        alleys[i + 1].bottomLeft()
//                );
//            } else {
//                mapObjectGraph.addEdge(
//                        alleys[i].topRight(),
//                        alleys[i + 1].topLeft()
//                );
//            }
//        }

        for (int i = 0; i < nalley - 1; i++) {
            Alley first = alleys[i];
            Alley second = alleys[i + 1];
            mapObjectGraph.addEdge(first.bottomRight(), second.bottomLeft());
            mapObjectGraph.addEdge(second.bottomLeft(), first.bottomRight());
            mapObjectGraph.addEdge(first.topRight(), second.topLeft());
            mapObjectGraph.addEdge(second.topLeft(), first.topRight());
        }

        for (MapObject mapObject: mapObjectGraph.vertexSet()) {
            mapObjectHashtable.put(mapObject.getId(), mapObject);
        }
    }

    private Alley[] createAlleys() {
        Alley[] alleys = new Alley[nalley];
        for (int i = 0; i < nalley; i++) {
            AlleyMovementDirection a = i % 2 == 0? AlleyMovementDirection.DOWNWARD : AlleyMovementDirection.UPWARD;
            alleys[i] = new Alley(i, nrow, a);
        }
        return alleys;
    }
}
